#!/usr/bin/env sh
#
# lock-n-pause.sh

# Re-activate bluetooth when unlocking
trap "${HOME}/.scripts/bton" EXIT

# Pause music
playerctl pause
${HOME}/.scripts/lock.sh
