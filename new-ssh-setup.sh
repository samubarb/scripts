#!/usr/bin/env bash
# new-ssh-setup.sh Tool to auto set up ssh key

# set -e

function error_keygen() {
    echo "Error generating key. Exiting..."
    exit 1
}

printf "Insert IP address or DNS name of the machne you want to login to: "
read dns
printf "Insert the Username for the machine: "
read usr
printf "Generate a new exclusive key? Default is '~/.ssh/id_rsa.pub' [y/N]: "
read ans

# Default key
key="id_rsa"

# Creating key pair if needed
case $ans in
    y|yes|Y|Yes|YES)
    printf "Name of the new key pair: "
    read key
    
    while [[ $key == "id_rsa" || $key == "id_rsa.pub" || $key == "" ]]; do
        echo "Error: key cannot be named with default name 'id_rsa'."
        printf "Name of the new key pair: "
        read key
    done

    ssh-keygen -f "${HOME}/.ssh/${key}" -C "Key pair for ${usr}@${dns}" -P "" >/dev/null && \
      echo "Key pair generated in '${HOME}/.ssh/${key}(.pub)'." || error_keygen
    ;;
    *)
    echo "Using default '~/.ssh/id_rsa.pub'"
    ;;
esac

# Copying key into remote machine
ssh-copy-id -i "${HOME}/.ssh/${key}.pub" "${usr}@${dns}"

# Add alias in ~/.ssh/config automatically
