#!/usr/bin/env sh
#
# Turns off bluetooth.service after 5 min from boot if nothing's connected

status=$(systemctl is-active bluetooth.service)

if [ $status == "active" ]; then
    
    # Number of connected devices
    devices=$(bluetoothctl devices | sed 's/.*\(..:..:..:..:..:..\).*/\1/' | xargs -n1 bluetoothctl info | grep "Connected: yes" | wc -l) 

    if [ $devices == "0" ]; then
        $HOME/.scripts/btoff
    fi
fi
