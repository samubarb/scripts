#!/usr/bin/env sh

param=("--insidecolor=0000001c" "--ringcolor=0000003e" \
        "--linecolor=00000000" "--keyhlcolor=ffffff80" "--ringvercolor=739122bb" \
        "--separatorcolor=22222260" "--insidevercolor=739122bb" \
        "--ringwrongcolor=880000bb" "--insidewrongcolor=880000bb" \
        "--verifcolor=ffffff00" "--wrongcolor=ff000000" "--timecolor=ffffff00" \
        "--datecolor=ffffff00" "--layoutcolor=ffffff00")

i3lock_cmd=(i3lock --blur 5)
# i3lock_cmd+=(--nofork)

# try to use i3lock with prepared parameters
if ! "${i3lock_cmd[@]}" "${param[@]}" >/dev/null 2>&1; then
    # We have failed, lets get back to stock one
    "${i3lock_cmd[@]}"
fi
