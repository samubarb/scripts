#!/usr/bin/env sh
#
# cmatrix-lock.sh

# Remember to add in compton.conf an opacity-rule as follows:
#
# opacity-rule = [
#     "0:name = 'i3lock'"
# ];

# Pause the music [optional]
playerctl pause

# Using i3-msg exec to create a non-blocking process
i3-msg exec "tilix --full-screen -e cmatrix"
# Launch i3lock
i3lock -n -u
# Close the terminal with cmatrix
killall cmatrix
