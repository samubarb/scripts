#!/usr/bin/env sh

scripts=$HOME/.scripts
$scripts/google-earth-wallpaper.sh | head -1 | xargs $scripts/set-wallpaper
