#!/usr/bin/env bash

echo "Stopping all containers..."
sudo docker stop $(sudo docker ps -a -q)
echo ""
echo "Removing all containers with relative volumes..."
sudo docker rm --volumes $(sudo docker ps -a -q)
echo ""
echo "Removing all remaining volumes..."
sudo docker volume rm $(sudo docker volume ls -q)
echo ""
echo "Clean."
