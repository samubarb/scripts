#!/usr/bin/env sh

BASE_DIR="${HOME}/Downloads/kafka"
CONFIG_DIR="${BASE_DIR}/config"
BIN_DIR="${BASE_DIR}/bin"

# Start Zookeeper
${BIN_DIR}/zookeeper-server-start.sh ${CONFIG_DIR}/zookeeper.properties &

echo "Waiting for Zookeeper..."
sleep 5

# Start Kafka
${BIN_DIR}/kafka-server-start.sh ${CONFIG_DIR}/server.properties &
