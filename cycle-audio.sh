#!/usr/bin/env sh

built_in_card="alsa_card.pci-0000_00_1b.0"
hdmi_card="alsa_card.pci-0000_00_03.0"
bluetooth_card="pactl set-card-profile bluez_card.78_44_05_96_CB_61"

MODES=("BUILT_IN_AUDIO" "HDMI_AUDIO" "BLUETOOTH")
TMP_FILE="/tmp/audio_mode.dat"

function set_built_in_audio() {
    pactl set-card-profile "${built_in_card}" output:analog-stereo+input:analog-stereo
    pactl set-card-profile "${hdmi_card}" off
    pactl set-card-profile "${bluetooth_card}" off
}

function set_hdmi_audio() {
    pacmd set-card-profile "${hdmi_card}" output:hdmi-stereo
    pacmd set-card-profile "${built_in_card}" off
    pactl set-card-profile "${bluetooth_card}" off
}

function set_bluetooth_audio() {
    pactl set-card-profile "${bluetooth_card}" a2dp_sink
    pactl set-card-profile "${hdmi_card}" off
    pacmd set-card-profile "${built_in_card}" off
}

if [ ! -f "/tmp/audio_mode.dat" ]; then
    audio_mode="${MODES[-1]}"
else
    audio_mode=`cat "${TMP_FILE}"`
fi

if [ $audio_mode = "HDMI_AUDIO" ]; then
    audio_mode="BUILT_IN_AUDIO"
    set_built_in_audio
elif [ $audio_mode = "BUILT_IN_AUDIO" ]; then
    audio_mode="BLUETOOTH"
    set_bluetooth_audio
elif [ $audio_mode = "BLUETOOTH" ]; then
    audio_mode="HDMI_AUDIO"
    set_hdmi_audio
else
    exit 1
fi

echo "${audio_mode}" > "${TMP_FILE}"
