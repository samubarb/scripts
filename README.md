# my-scripts

Miscellaneous scripts that I use to automate some processes. Mainly in bash, but not only.

## multi_monitor.sh
If an external monitor is connected, the script puts the external screen above the internal one. After that it puts all the odd workspaces in the external monitor and the even ones in the internal.