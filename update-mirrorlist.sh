#!/usr/bin/env bash

workdir="/etc/pacman.d"
ref="mirrorlist"
new="mirrorlist.pacnew"
backup="mirrorlist.backup"

error_file() {
    echo "ERROR! Missing file: check if '${ref}' and '${new}' exist in '${workdir}/'."
    exit 1
}

error_rank() {
    echo "ERROR! Something went wrong during mirror's ranking."
    exit 1
}

success_rank() {
    echo "SUCCESS! Ranked mirror list saved in ${workdir}/${ref}."
    exit 0
}

# Checking needed files existance
[ -f "${workdir}/${new}" ] && [ -f "${workdir}/${ref}" ] || error_file

echo "Backing up: ${ref} -> ${backup}..."
sudo cp "${workdir}/${ref}" "${workdir}/${backup}"
echo "Testing new mirrors in ${new} (will take a while)..."

# Uncomment all mirrors
sudo sed -i 's/^#Server/Server/' "${workdir}/${new}"

# Rank all mirrors and keep the first six
sudo rankmirrors -n 6 "${workdir}/${new}" > "${workdir}/${ref}" && success_rank || error_rank
