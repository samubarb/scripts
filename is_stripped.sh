#!/bin/bash

# Returns:
#  - 'yes' if the executable has no symbols (stripped)
#  - 'no' if the executable contains debug symbols (not stripped)
#  - exit status '1' and relative error message (no such file, file format not recognized)

file=${1}
output=$(nm "${1}" 2>&1)
return_code="$?" # nm exit code

if [ "${return_code}" == 0 ]; then
    echo "${output}" | grep "no symbols" > /dev/null
    return_code="$?" # grep exit code
    [ "${return_code}" == 0 ] && echo "yes" || echo "no"
else
    echo "${output}"
    exit 1
fi
