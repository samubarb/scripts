#!/usr/bin/env sh

kill "$(pstree -lp | grep -- dwmbar.sh | sed "s/.*sleep(\([0-9]\+\)).*/\1/")" &>/dev/null
echo "dwm bar refreshed."
