#!/usr/bin/env bash

# Stop script on error
set -e

FILE_PATH=${1}
FLASH_TTY=${2:-/dev/ttyUSB1}
BAUD_RATE=${3:-9600}
MEMORY_SIZE_BYTES=${4:-32768}

### Input validation ###

if [ -z "${FILE_PATH}" ]; then echo "Missing input file."; exit 1; fi
FILE_SIZE_BYTES=$(du --bytes "${FILE_PATH}" | cut -f1)
if [ "${FILE_SIZE_BYTES}" -gt "${MEMORY_SIZE_BYTES}" ]; then echo "File '${FILE_PATH}' is larger than '${MEMORY_SIZE_BYTES}' bytes."; exit 1; fi
if [ ! -f "${FILE_PATH}" ]; then echo "File path '${FILE_PATH}' not found."; exit 1; fi
if [ ! -e "${FLASH_TTY}" ]; then echo "TTY '${FLASH_TTY}' not found."; exit 1; fi
if [ ! -w "${FLASH_TTY}" ]; then echo "No writing permission on '${FLASH_TTY}', try with sudo."; exit 1; fi

### Setting up TTY ###

stty --file "${FLASH_TTY}" "${BAUD_RATE}" -echo -icanon -opost -onlcr -cstopb -parenb

### Show TTY settings ###

# stty --file "${FLASH_TTY}" --all

### Flashing ###

echo "Start flashing '${FILE_PATH}' through ${FLASH_TTY}..."

{
    dd if="${FILE_PATH}" status=none
    dd if=/dev/zero count=1 bs="$((MEMORY_SIZE_BYTES - FILE_SIZE_BYTES))" status=none
} | pv --size "${MEMORY_SIZE_BYTES}" --stop-at-size --width 80 > "${FLASH_TTY}"

echo "Done flashing."

### Notes ###

# -echo   | disable echo input characters
# -icanon | disable special characters: erase, kill, werase, rprnt
# -opost  | disable postprocess output
# -onlcr  | don't translate newline to carriage return-newline
# -cstopb | use one stop bit per character (two with 'cstopb' without `-`)
# -parenb | don't generate parity bit in output (and don't expect parity bit in input)
# --all   | print all current settings in human-readable form
