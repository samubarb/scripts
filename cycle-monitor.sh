#!/usr/bin/env sh

INTERNAL=("0" "eDP1")
EXTERNAL_HDMI=("1" "DP1")
EXTERNAL_VGA=("2" "VGA1")

id="0"
name="1"

BEHAVIOUR=${1-"--cycle"} # ${1} can be "--set" or "--cycle"
MODES=("EXTERNAL_ONLY" "INTERNAL_ONLY" "INTERNAL_RIGHT" "INTERNAL_BELOW")
TMP_FILE="/tmp/monitor_mode.dat"

function intOnly() {
    monitor_mode="INTERNAL_ONLY"
    xrandr --output eDP1 --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI1 --off --output VGA1 --off --output VIRTUAL1 --off
}

function extOnly() {
    monitor_mode="EXTERNAL_ONLY"
    mons -O "${EXTERNAL_HDMI[$id]}" --primary "${EXTERNAL_HDMI[$name]}" || \
        mons -O "${EXTERNAL_VGA[$id]}" --primary "${EXTERNAL_VGA[$name]}" || \
        return 1
}

function intRight() {
    monitor_mode="INTERNAL_RIGHT"
    mons -S "${EXTERNAL_HDMI[$id]},${INTERNAL[$id]}:R" --primary "${EXTERNAL_HDMI[$name]}" || \
        mons -S "${EXTERNAL_VGA[$id]},${INTERNAL[$id]}:R" --primary "${EXTERNAL_VGA[$name]}" || \
        return 1
}

function intBelow() {
    monitor_mode="INTERNAL_BELOW"
    mons -S "${INTERNAL[$id]},${EXTERNAL_HDMI[$id]}:T" --primary "${EXTERNAL_HDMI[$name]}" || \
        mons -S "${INTERNAL[$id]},${EXTERNAL_VGA[$id]}:T" --primary "${EXTERNAL_VGA[$name]}" || \
        return 1
}

if [ $BEHAVIOUR = "--set" ]; then
    if [ ${2} = "EXTERNAL_ONLY" ]; then
        extOnly || fallBack
    elif [ ${2} = "INTERNAL_ONLY" ]; then
        intOnly
    elif [ ${2} = "INTERNAL_RIGHT" ]; then
        intRight || fallBack
    elif [ ${2} = "INTERNAL_BELOW" ]; then
        intBelow || fallBack
    else
        exit 1
    fi

elif [ $BEHAVIOUR = "--cycle" ]; then
    # if we don't have a file, set the last mode as the past one
    if [ ! -f "${TMP_FILE}" ]; then
        monitor_mode="${MODES[-1]}"
    # otherwise read the value from the file
    else
        monitor_mode=`cat /tmp/monitor_mode.dat`
    fi

    if [ $monitor_mode = "${MODES[-1]}" ]; then
        extOnly || fallBack
    elif [ $monitor_mode = "${MODES[0]}" ]; then
        intOnly
    elif [ $monitor_mode = "${MODES[1]}" ]; then
        intRight || fallBack
    elif [ $monitor_mode = "${MODES[2]}" ]; then
        intBelow || fallBack
    else
        exit 1
    fi

else
    exit 1
fi

${HOME}/.scripts/set-wallpaper &
echo "${monitor_mode}" > "${TMP_FILE}"
