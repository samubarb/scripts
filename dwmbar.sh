#!/bin/sh

delim=" | "
status_scripts="${HOME}/.scripts/statusbar"

function show() {
    if [ "${2}" == "--first" ]; then
        ${status_scripts}/${1}; echo ${delim}
    elif [ "${2}" == "--last" ]; then
        ${status_scripts}/${1}
    elif [ "${2}" == "--optional" ]; then
        if [ "$(${status_scripts}/${1} | tr '\n' ' ')" != " " ]; then
            ${status_scripts}/${1}; echo ${delim}
        fi
    else
        ${status_scripts}/${1}; echo ${delim}
    fi
}

function status() {
    printf " "
    show "spotify" --optional
    # show "pacpackages" --first
    show "bluetooth" --first
    # show "weather" --optional
    show "openvpn-status" --optional
    show "volume"
    show "clock"
    show "battery" --last
}

while :; do
    xsetroot -name "$(status | tr '\n' ' ')"
    sleep 30s
done
