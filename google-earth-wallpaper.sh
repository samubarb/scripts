#!/usr/bin/env sh

folder="$HOME/Pictures/google-earth-wallpaper"
domain="https://earthview.withgoogle.com/"
max_img_num=10
verbose="False"

# Checking verbose flag
[[ ${1} == "--verbose" || ${1} == "-v" ]] && verbose="True"

# Create folder if doesn't exist
mkdir -p $folder

# Download the image
[ $verbose == "True" ] && echo "Attempting new image download."
wget -nd -r -P $folder -A jpeg,jpg $domain &>/dev/null
wget_exit_status=$?

# If more than $max_img_num, then remove the older one
num=$(ls -1 $folder | wc -l)
if [ $num -gt $max_img_num ]; then
    [ $verbose == "True" ] && echo "${num}/${max_img_num} images. Deleting the oldest one."
    to_delete=$(ls -t $folder | tail $(expr $max_img_num - $num) | tr "\n" " ")
    for f in ${to_delete[@]}; do
        rm $folder/"$f"
    done
fi

# If download went ok, then set the last image as wallpaper
if [ $wget_exit_status -eq 0 ]; then
    [ $verbose == "True" ] && echo "Download succeeded. Setting just downloaded image."
    wallpaper_img=$(ls -t $folder | head -1)
# Else, select a random image from the saved ones
else
    [ $verbose == "True" ] && echo "Download failed. Setting random existing image."
    folder_content=($(ls -t $folder))
    rand=$(expr $RANDOM % $max_img_num)
    wallpaper_img=${folder_content[$rand]}
fi

echo "$folder/$wallpaper_img"
